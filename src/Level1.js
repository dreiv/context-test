import React, { useContext } from 'react'
import { AppContext } from './AppContext'
import Level2 from './Level2'

export default () => {
	const { val, setVal } = useContext(AppContext)

	const handleChange = evt => {
		setVal(evt.target.value)
	}

	return (
		<div className="level1">
			<h1>Level 1</h1>
			<input
				type="number"
				value={val}
				onChange={handleChange}
			/>

			<Level2 />
		</div>
	)
}
