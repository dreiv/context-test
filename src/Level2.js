import React, { useContext } from 'react'
import { AppContext } from './AppContext'

export default () => {
	const { val } = useContext(AppContext)

	return (
		<div className="level2">
			<h1>Level 2</h1>

			<div>Context Value: {val}</div>
		</div>
	)
}
