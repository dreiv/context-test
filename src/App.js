import React from 'react';
import './App.css'
import Level1 from './Level1'

function App() {
	return (
		<div class="app">
			Base component
			<Level1 />
		</div>
	)
}

export default App;
