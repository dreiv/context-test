import React, { useState } from 'react'

export const AppContext = React.createContext()
export default ({ children }) => {
	const [val, setVal] = useState(2)

	const defaultContext = {
		val,
		setVal,
	}

	return (
		<AppContext.Provider value={defaultContext}>
			{children}
		</AppContext.Provider>
	)
}